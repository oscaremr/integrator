package org.oscarehr.caisi_integrator.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

public class JpaUtils
{
	private static EntityManagerFactory entityManagerFactory = SpringUtils.getBean(EntityManagerFactory.class);

	/**
	 * This method will close the entity manager.
	 * Any active transaction will be rolled back.
	 */
	public static void close(EntityManager entityManager)
	{
		EntityTransaction tx = entityManager.getTransaction();
		if (tx != null && tx.isActive())
		{
			tx.rollback();
		}
		entityManager.close();
	}

	public static EntityManager createEntityManager()
	{
		MiscUtils.getLogger().info("Entity Manager Factory " + entityManagerFactory);
		return entityManagerFactory.createEntityManager();
	}
}
