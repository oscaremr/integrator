package org.oscarehr.caisi_integrator.ws.transfer;

import java.io.Serializable;
import java.util.Date;

public class SetConsentTransfer implements Serializable
{
	private Integer demographicId;
	private String consentStatus;
	private Date createdDate;
	private boolean excludeMentalHealthData;
	private FacilityConsentPair[] consentToShareData;
	private Date expiry;

	public Integer getDemographicId()
	{
		return demographicId;
	}

	public void setDemographicId(Integer demographicId)
	{
		this.demographicId = demographicId;
	}

	public String getConsentStatus()
	{
		return consentStatus;
	}

	public void setConsentStatus(String consentStatus)
	{
		this.consentStatus = consentStatus;
	}

	public Date getCreatedDate()
	{
		return createdDate;
	}

	public void setCreatedDate(Date createdDate)
	{
		this.createdDate = createdDate;
	}

	public boolean isExcludeMentalHealthData()
	{
		return excludeMentalHealthData;
	}

	public void setExcludeMentalHealthData(boolean excludeMentalHealthData)
	{
		this.excludeMentalHealthData = excludeMentalHealthData;
	}

	public FacilityConsentPair[] getConsentToShareData()
	{
		return consentToShareData;
	}

	public void setConsentToShareData(FacilityConsentPair[] consentToShareData)
	{
		this.consentToShareData = consentToShareData;
	}

	public Date getExpiry()
	{
		return expiry;
	}

	public void setExpiry(Date expiry)
	{
		this.expiry = expiry;
	}

}
