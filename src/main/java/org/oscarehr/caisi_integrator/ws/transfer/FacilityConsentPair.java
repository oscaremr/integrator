package org.oscarehr.caisi_integrator.ws.transfer;

public class FacilityConsentPair
{
	private Integer remoteFacilityId;
	private boolean shareData;

	public Integer getRemoteFacilityId()
	{
		return remoteFacilityId;
	}

	public void setRemoteFacilityId(Integer remoteFacilityId)
	{
		this.remoteFacilityId = remoteFacilityId;
	}

	public boolean isShareData()
	{
		return shareData;
	}

	public void setShareData(boolean shareData)
	{
		this.shareData = shareData;
	}
}
