package org.oscarehr.caisi_integrator.dao;

import java.util.Comparator;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.PostLoad;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.openjpa.persistence.jdbc.Index;

@Entity
public class CachedDemographicNote extends AbstractModel<CachedDemographicNoteCompositePk>
{

	public static final Comparator<CachedDemographicNote> OBSERVATION_DATE_COMPARATOR = new Comparator<CachedDemographicNote>()
	{
		@Override
		public int compare(CachedDemographicNote o1, CachedDemographicNote o2)
		{
			return(o1.getObservationDate().compareTo(o2.getObservationDate()));
		}
	};

	@EmbeddedId
	@Index
	private CachedDemographicNoteCompositePk cachedDemographicNoteCompositePk;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date updateDate = null;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date observationDate = null;

	@Column(nullable = false)
	private int caisiDemographicId = 0;

	@Column(nullable = false, length = 16)
	private String observationCaisiProviderId = null;

	@Column(length = 16)
	private String signingCaisiProviderId = null;

	@Column(nullable = false, length = 100)
	private String encounterType = null;

	@Column(nullable = false)
	private int caisiProgramId = 0;

	@Column(columnDefinition = "mediumtext")
	private String note = null;

	@Column(nullable = false, length = 64)
	private String role = null;

	@ElementCollection(targetClass = String.class, fetch = FetchType.EAGER)
	@CollectionTable(name = "CachedDemographicNoteIssues", joinColumns = { @JoinColumn(name = "integratorFacilityId", referencedColumnName = "integratorFacilityId"), @JoinColumn(name = "uuid", referencedColumnName = "uuid") })
	@Column(name = "noteIssue")
	private Set<String> noteIssues = new HashSet<String>();

	@Transient
	private Set<NoteIssue> issues = new HashSet<NoteIssue>();

	public CachedDemographicNote()
	{
		//Default
	}

	public CachedDemographicNote(Integer integratedFacilityId, String uuid, int caisiDemographicId, int caisiProgramId, String observationCaisiProviderId, Date observationDate, String role)
	{

		this.cachedDemographicNoteCompositePk = new CachedDemographicNoteCompositePk(integratedFacilityId, uuid);
		this.caisiDemographicId = caisiDemographicId;
		this.caisiProgramId = caisiProgramId;
		this.observationCaisiProviderId = observationCaisiProviderId;
		this.observationDate = observationDate;
		this.role = role;
	}

	public void setCachedDemographicNoteCompositePk(CachedDemographicNoteCompositePk cachedDemographicNoteCompositePk)
	{
		this.cachedDemographicNoteCompositePk = cachedDemographicNoteCompositePk;
	}

	public CachedDemographicNoteCompositePk getCachedDemographicNoteCompositePk()
	{
		return cachedDemographicNoteCompositePk;
	}

	@Override
	public CachedDemographicNoteCompositePk getId()
	{
		return cachedDemographicNoteCompositePk;
	}

	public Date getUpdateDate()
	{
		return updateDate;
	}

	public void setUpdateDate(Date updateDate)
	{
		this.updateDate = updateDate;
	}

	public Date getObservationDate()
	{
		return observationDate;
	}

	public void setObservationDate(Date observationDate)
	{
		this.observationDate = observationDate;
	}

	public int getCaisiDemographicId()
	{
		return caisiDemographicId;
	}

	public void setCaisiDemographicId(int caisiDemographicId)
	{
		this.caisiDemographicId = caisiDemographicId;
	}

	public String getObservationCaisiProviderId()
	{
		return observationCaisiProviderId;
	}

	public void setObservationCaisiProviderId(String observationCaisiProviderId)
	{
		this.observationCaisiProviderId = observationCaisiProviderId;
	}

	public int getCaisiProgramId()
	{
		return caisiProgramId;
	}

	public void setCaisiProgramId(int caisiProgramId)
	{
		this.caisiProgramId = caisiProgramId;
	}

	public String getRole()
	{
		return role;
	}

	public void setRole(String role)
	{
		this.role = role;
	}

	public String getNote()
	{
		return note;
	}

	public void setNote(String note)
	{
		this.note = note;
	}

	public String getSigningCaisiProviderId()
	{
		return signingCaisiProviderId;
	}

	public void setSigningCaisiProviderId(String signingCaisiProviderId)
	{
		this.signingCaisiProviderId = signingCaisiProviderId;
	}

	public String getEncounterType()
	{
		return encounterType;
	}

	public void setEncounterType(String encounterType)
	{
		this.encounterType = encounterType;
	}

	public Set<NoteIssue> getIssues()
	{
		return this.issues;
	}

	public void setIssues(Set<NoteIssue> issues)
	{
		this.issues = issues;
	}

	@XmlTransient
	public Set<String> getNoteIssues()
	{
		return noteIssues;
	}

	public void setNoteIssues(Set<String> noteIssues)
	{
		this.noteIssues = noteIssues;
	}

	@PostLoad
	protected void logRead()
	{
		for (String noteIssue : noteIssues)
		{
			getIssues().add(NoteIssue.valueOf(noteIssue));
		}
	}

	@PreUpdate
	@PrePersist
	protected void logWrite()
	{
		for (NoteIssue issue : issues)
		{
			getNoteIssues().add(issue.asString());
		}
	}

}
