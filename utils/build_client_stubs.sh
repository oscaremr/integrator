#!/bin/sh

rm -rf ../target/client_stubs
rm -rf ../target/client_stubs_classes

for foo in `cat services.txt` 
do
	echo $foo
	ant -f build_client_stubs.xml -DserviceName=${foo}
done

#rm -f ~/.m2/repository/org/oscarehr/caisi_integrator/caisi_integrator_client_stubs/SNAPSHOT/*.jar

## Install the client stubs in the local maven repository
mvn install:install-file -DgroupId=org.oscarehr.caisi_integrator -DartifactId=caisi_integrator_client_stubs -Dversion=1.1 -Dpackaging=jar -Dfile=../target/caisi_integrator_client_stubs.jar -DcreateChecksum=true -DgeneratePom=true 

## Install the dependencies into the local maven repository
#mvn install:install-file -DgroupId=org.oscarehr.caisi_integrator -DartifactId=integrator-objects -Dversion=0.0.4 -Dpackaging=jar -Dfile=../target/integrator-objects.jar -DcreateChecksum=true -DgeneratePom=true

